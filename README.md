# Antre mystère

Clone with HTTPS:
```shell
git clone https://gitlab.com/eternalfest/games/antre-mystere.git
```

Clone with SSH:
```shell
git clone git@gitlab.com:eternalfest/games/antre-mystere.git
```
