package event_timer.actions;

import hf.Hf;
import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;
import etwin.ds.WeakMap;

class TimerAdd implements IAction {
  public var name(default, null): String = Obfu.raw("timerAdd");
  public var isVerbose(default, null): Bool = false;
    
  private var mod: EventTimer;

  public function new(mod: EventTimer) {
    this.mod = mod;
  }

  public function run(ctx: IActionContext): Bool {
    var minutes: Int = ctx.getOptInt(Obfu.raw("min")).or(0);
    var secondes: Int = ctx.getOptInt(Obfu.raw("s")).or(0);
    var game: GameMode = ctx.getGame();

    var secondesTimer = EventTimer.TIMER_SECONDES.get(game);
    EventTimer.TIMER_SECONDES.set(game, secondesTimer + secondes);
      
    while(true) {
        
      if(EventTimer.TIMER_SECONDES.get(game) > 59 || EventTimer.TIMER_SECONDES.get(game) < 0) {
        if(EventTimer.TIMER_SECONDES.get(game) > 59) {
          minutes++;
          secondesTimer = EventTimer.TIMER_SECONDES.get(game);
          EventTimer.TIMER_SECONDES.set(game, secondesTimer - 60); 
        }
        if(EventTimer.TIMER_SECONDES.get(game) < 0) {
          minutes--;
          secondesTimer = EventTimer.TIMER_SECONDES.get(game);
          EventTimer.TIMER_SECONDES.set(game, secondesTimer + 60); 
        }
      } else {
        break;
      }
        
    }
      
    var minutesTimer = EventTimer.TIMER_MINUTES.get(game);
    EventTimer.TIMER_MINUTES.set(game, minutesTimer + minutes); 
    
    if(EventTimer.TIMER_MINUTES.get(game) < 0) {
      EventTimer.TIMER_MINUTES.set(game, 0);
      EventTimer.TIMER_SECONDES.set(game, 0);
      EventTimer.TIMER_CENTIEMES.set(game, 0);
    }
      
    return false;
  }
}