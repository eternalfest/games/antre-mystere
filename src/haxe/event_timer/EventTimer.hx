package event_timer;

import patchman.IPatch;
import patchman.Ref;
import merlin.IAction;
import etwin.ds.FrozenArray;
import etwin.Obfu;
import etwin.flash.MovieClip;
import etwin.ds.WeakMap;
import hf.Hf;
import hf.mode.GameMode;
import hf.FxManager;
import hf.SpecialManager;
import hf.entity.Player;
import pieces_violettes.PiecesViolettes;

import event_timer.actions.TimerStart;
import event_timer.actions.TimerStop;
import event_timer.actions.TimerReinit;
import event_timer.actions.TimerHasEnded;
import event_timer.actions.TimerAdd;

@:build(patchman.Build.di())
class EventTimer {

  @:diExport
  public var actions(default, null): FrozenArray<IAction>;
  @:diExport
  public var patches(default, null): FrozenArray<IPatch>;
  
  public var hasBeenTraced = false;
  public var timerEnd = false;
  public static var TIMER_ACTIVATE: WeakMap<GameMode, Bool> = new WeakMap();
  public static var TIMER_LEVEL: WeakMap<GameMode, Bool> = new WeakMap();
  public static var TIMER_EFFECTS: WeakMap<GameMode, Bool> = new WeakMap();
  public static var TIMER_MINUTES: WeakMap<GameMode, Float> = new WeakMap();
  public static var TIMER_SECONDES: WeakMap<GameMode, Float> = new WeakMap();
  public static var TIMER_CENTIEMES: WeakMap<GameMode, Float> = new WeakMap();
  public static var TIMER_TEXT_SCALE: WeakMap<GameMode, Int> = new WeakMap();
  public static var TIMER_MAX_TEXT_SCALE: WeakMap<GameMode, Int> = new WeakMap();
  public static var TIMER_HAS_ENDED: WeakMap<GameMode, Bool> = new WeakMap();

  public function new() {
    this.actions = FrozenArray.of(
      new TimerStart(this),
      new TimerStop(this),
      new TimerReinit(this),
      new TimerHasEnded(this),
      new TimerAdd(this)
    );

    var patches = [
        
      Ref.auto(hf.entity.Player.onDeathLine).wrap(function(hf, self: Player, old): Void {
        if(!self.fl_kill && self.game.checkLevelClear()) {
          if(TIMER_ACTIVATE.get(self.game) && TIMER_LEVEL.get(self.game)) {
            TIMER_ACTIVATE.set(self.game, false);
            self.game.shake(0, 0);
            this.timerRemove(hf, self.game.fxMan);
          }
        }
        old(self);
      }),
        
      Ref.auto(hf.SpecialManager.warpZone).wrap(function(hf, self: SpecialManager, w: Int, old): Void {
        if(TIMER_ACTIVATE.get(self.game) && TIMER_LEVEL.get(self.game)) {
          TIMER_ACTIVATE.set(self.game, false);
          self.game.shake(0, 0);
          this.timerRemove(hf, self.game.fxMan);
        }
        old(self, w);
      }),
        
      Ref.auto(hf.mode.GameMode.main).wrap(function(hf, self: GameMode, old): Void {
        if(TIMER_ACTIVATE.get(self)) {
          if(!self.fl_pause) {
            if(TIMER_TEXT_SCALE.get(self) > TIMER_MAX_TEXT_SCALE.get(self)) {
              TIMER_TEXT_SCALE.set(self, TIMER_TEXT_SCALE.get(self) - 10);
            }
            if(TIMER_EFFECTS.get(self)) {
              var choix = 0;
              var nombre = 0;
              if(TIMER_MINUTES.get(self) < 4) {
                choix = 4;
                nombre = 1;
                //self.shake(10, 1);
                if(TIMER_MINUTES.get(self) < 2) {
                  choix = 8;
                  nombre = 1;
                  //self.shake(10, 2);
                  if(TIMER_MINUTES.get(self) < 1) {
                    choix = 15;
                    nombre = 1;
                    //self.shake(10, 3);
                    if(TIMER_SECONDES.get(self) < 10) {
                      choix = 20;
                      nombre = 2;
                      self.shake(10, 1);
                    }
                  }
                }
              }
              var choix2 = Std.random(100);
              for(i in 0...nombre+1) {
                if(choix2 < choix) {
                  self.fxMan.inGameParticles(hf.Data.PARTICLE_STONE, Std.random(hf.Data.GAME_WIDTH), Std.random(hf.Data.GAME_HEIGHT), Std.random(2));
                }
              }
            }
            TIMER_CENTIEMES.set(self, TIMER_CENTIEMES.get(self) - 100/40);
            TIMER_CENTIEMES.set(self, Math.floor(TIMER_CENTIEMES.get(self)));
            if(TIMER_CENTIEMES.get(self) < 0) {
              TIMER_CENTIEMES.set(self, Math.floor(100*(1 - 1/40)));
              TIMER_SECONDES.set(self, TIMER_SECONDES.get(self) - 1);
              if(TIMER_MINUTES.get(self) < 1 && TIMER_SECONDES.get(self) < 10) {
                TIMER_MAX_TEXT_SCALE.set(self, TIMER_MAX_TEXT_SCALE.get(self) + 10);
                TIMER_TEXT_SCALE.set(self, TIMER_TEXT_SCALE.get(self) + 80);
                if(PiecesViolettes.PIECES_ACTIVATE.get(self)) {
                  TIMER_TEXT_SCALE.set(self, 150);
                }
              }
            }
            if(TIMER_SECONDES.get(self) < 0) {
              TIMER_SECONDES.set(self, 59);
              TIMER_MINUTES.set(self, TIMER_MINUTES.get(self) - 1);
              TIMER_TEXT_SCALE.set(self, TIMER_TEXT_SCALE.get(self) + 80);
            }
            if(TIMER_MINUTES.get(self) < 0) {
              this.timerEnd = true;
              TIMER_MINUTES.set(self, 0);
              TIMER_SECONDES.set(self, 0);
              TIMER_CENTIEMES.set(self, 0);
              TIMER_ACTIVATE.set(self, false);
            }
            this.timerDisplay(hf, self.fxMan, TIMER_TEXT_SCALE.get(self));
          }
        }
        if(this.timerEnd) {
          TIMER_HAS_ENDED.set(self, true);
          /* TODO for mauso
				if (!hasBeenTraced) {

					this.lock();
					this.world.lock();
					// Std.addEmptyMovie("square_mc", Std.getNextHighestDepth());
					this.square_mc = this.game.scene.empty(this.getNextHighestDepth());
					this.square_mc.beginFill(0xFFFFFF);
					this.square_mc._alpha = 0;
					this.square_mc.moveTo(0, 0);
					this.square_mc.lineTo(0, 520);
					this.square_mc.lineTo(420, 520);
					this.square_mc.lineTo(420, 0);
					this.square_mc.lineTo(0, 0);
					this.square_mc.endFill();
					hasBeenTraced = true;
				}
				else
				{
					this.square_mc._alpha = this.square_mc._alpha + 0.9;
				}
						
				this.shake(10, 5);

				if (this.isFlippedX) {
					this.rt._x = datas.LEVEL_WIDTH + this.gameXOffset - Math.round((random(2) * 2 - 1) * (random(Math.round(this["shakeIntensity"] * 10)) / 10) * this["shakeTimer"] / this["shakeTimerTotal"]);
				} else {
					this.rt._x = Math.round(this.gameXOffset + (random(2) * 2 - 1) * (random(Math.round(this["shakeIntensity"] * 10)) / 10) * this["shakeTimer"] / this["shakeTimerTotal"]);
				}
				if (this.isFlippedY) {
					this.rt._y = datas.LEVEL_HEIGHT + 20 + Math.round(this.gameYOffset + (random(2) * 2 - 1) * (random(Math.round(this["shakeIntensity"] * 10)) / 10) * this["shakeTimer"] / this["shakeTimerTotal"]);
				} else {
					this.rt._y = Math.round(this.gameYOffset + (random(2) * 2 - 1) * (random(Math.round(this["shakeIntensity"] * 10)) / 10) * this["shakeTimer"] / this["shakeTimerTotal"]);
							
					if (this.square_mc._alpha >= 100)
						this["onGameEnd"]();
				}
			*/
        }
        old(self);
      }),
         
    ];
      
    this.patches = FrozenArray.from(patches);
  }
    
  public function timerDisplay(hf: Hf, self: FxManager, textSize: Int): Void {
    var secondes: String = "";
    if(TIMER_SECONDES.get(self.game) < 10) secondes = "0";
    var centiemes: String = "";
    if(TIMER_CENTIEMES.get(self.game) < 10) centiemes = "0";
          
    self.igMsg.removeMovieClip();
    self.igMsg = cast self.game.depthMan.attach('hammer_interf_inGameMsg', hf.Data.DP_TOP);
    (cast self.igMsg).field._width = 400;
    (cast self.igMsg).label.text = "";
    (cast self.igMsg).field.text = TIMER_MINUTES.get(self.game)+":"+secondes+TIMER_SECONDES.get(self.game)+":"+centiemes+TIMER_CENTIEMES.get(self.game);
    (cast self.igMsg).field._xscale = textSize;
    (cast self.igMsg).field._yscale = textSize;
    (cast self.igMsg).field._x = 0;
    (cast self.igMsg).field._y = 0;
    if(TIMER_MINUTES.get(self.game) == 0 && TIMER_SECONDES.get(self.game) < 10) {
      (cast self.igMsg).field.textColor = 15597568;
    }
    hf.FxManager.addGlow(self.igMsg, 0, 2);
    (cast self.igMsg).timer = TIMER_SECONDES.get(self.game) * 5; 
  }
    
  public function timerMessageCentre(hf: Hf, self: FxManager, txt: String): MovieClip {
    var hud = cast self.game.depthMan.attach('hurryUp', hf.Data.DP_INTERF);
    (cast hud)._x = (hf.Data.GAME_WIDTH) / 2;
    (cast hud)._y = (hf.Data.GAME_HEIGHT) / 2;
    (cast hud).label = txt;
    (cast hud)._xscale = 100;
    (cast hud)._yscale = 100;
    self.mcList.push(hud);
    self.lastAlert = hud;
    return hud;
  }
    
  public function timerRemove(hf: Hf, self: FxManager): Void {
    self.igMsg.removeMovieClip();
  }
}