import bugfix.Bugfix;
import debug.Debug;
import hf.Hf;
import merlin.Merlin;
import vault.Vault;
import game_params.GameParams;
import shadow_clones.ShadowClones;
import patchman.IPatch;
import patchman.Patchman;
import atlas.Atlas;
import better_script.BetterScript;
import antre.Actions;
import antre.Items;
import antre.quetes.QuestChocorems;
import antre.quetes.QuestTabasco;
import antre.quetes.QuestRepas;
import antre.quetes.QuestNacho;
import antre.quetes.QuestSpeed;
import antre.quetes.QuestsInit;
import display_texts.DisplayTexts;
import disabled_controls.DisabledControls;
import zoom.Zoom;
import bads.Bads;
import marisa.Marisa;
import event_timer.EventTimer;
import pieces_violettes.PiecesViolettes;
import user_data.UserData;
import quests.QuestManager;
import etwin.Obfu;

@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(
    bugfix: Bugfix,
    debug: Debug,
	actions: Actions,
    items: Items,
    display_texts: DisplayTexts,
    disabled_controls: DisabledControls,
    zoom: Zoom,
    bads: Bads,
    marisa: Marisa,
    event_timer: EventTimer,
    pieces_violettes: PiecesViolettes,
    atlasDarkness: atlas.props.Darkness,
    atlas: atlas.Atlas,
    vault: Vault,
    game_params: GameParams,
    shadow_clones: ShadowClones,
    betterScript: BetterScript,
    questsInit: QuestsInit,
    quests: QuestManager,
    merlin: Merlin,
    user_data: UserData,
    patches: Array<IPatch>,
    hf: Hf
  ) {
    quests.register_reward(Obfu.raw("chocorems"), new QuestChocorems());
    quests.register_reward(Obfu.raw("tabasco"), new QuestTabasco());
    quests.register_reward(Obfu.raw("repas"), new QuestRepas());
    quests.register_reward(Obfu.raw("nacho"), new QuestNacho());
    quests.register_reward(Obfu.raw("speed"), new QuestSpeed());
    Patchman.patchAll(patches, hf);
  }
}