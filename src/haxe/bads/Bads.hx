package bads;

import hf.Hf;
import hf.Mode;
import hf.mode.GameMode;
import hf.Entity;
import hf.entity.Bad;
import hf.entity.bad.walker.Fraise;
import etwin.ds.FrozenArray;
import etwin.flash.MovieClip;
import etwin.flash.DynamicMovieClip;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

@:build(patchman.Build.di())
class Bads {

  @:diExport
  public var patches(default, null): IPatch;
    
  public static inline var ID_PIC_VERITABLE = 21;
  public static inline var ID_FAUSSE_FRAISE = 22;
  public var fruitId: Int;

  public function new() {
    this.patches = new PatchList([

      Ref.auto(hf.mode.GameMode.attachBad).wrap(function(hf, self: GameMode, id: Int, x: Float, y: Float, old): Bad {
        
        // Faux pic
        if(id == ID_PIC_VERITABLE) {
            
          fruitId = id;
          var cerise = hf.entity.bad.walker.Cerise.attach(self, x, y);
          var s = self.depthMan.attach("hammer_bad_spear", hf.Data.DP_BADS);
          var spear: DynamicMovieClip = cast s;
          spear.gotoAndStop("1");
          spear.sub.gotoAndStop("1");
          cerise.stick(spear, 0, 0);
          cerise._visible = false;
          return cerise;
        
        // Fausse fraise
        } else if(id == ID_FAUSSE_FRAISE) {
            
          fruitId = ID_FAUSSE_FRAISE;
          var fraise = hf.entity.bad.walker.Fraise.attach(self, x, y);
          return fraise;
            
        // Autres fruits
        } else {
          
          fruitId = id;
          return old(self, id, x, y);
            
        }

      }),
        
      Ref.auto(hf.entity.Bad.calcSpeed).wrap(function(hf, self: Bad, old): Void {
          if(fruitId == ID_PIC_VERITABLE) {
              self.speedFactor = 0;
          } else {
              old(self);
          }
      }),
      Ref.auto(hf.entity.Bad.updateSpeed).wrap(function(hf, self: Bad, old): Void {
          if(fruitId != ID_PIC_VERITABLE && self.speedFactor != 0) {
              old(self);
          }
      }),
        
      Ref.auto(hf.entity.bad.walker.Fraise.onShoot).wrap(function(hf, self: Fraise, old): Void {
        if(fruitId != ID_FAUSSE_FRAISE) {
          old(self);
        }
      }),
      Ref.auto(hf.entity.bad.walker.Fraise.startShoot).wrap(function(hf, self: Fraise, old): Void {
        if(fruitId != ID_FAUSSE_FRAISE) {
          old(self);
        }
      }),
      Ref.auto(hf.entity.bad.walker.Fraise.onEndAnim).wrap(function(hf, self: Fraise, id: Int, old): Void {
        if(fruitId != ID_FAUSSE_FRAISE) {
          old(self, id);
        }
      }),

    ]);
  }
}