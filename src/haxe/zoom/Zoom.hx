package zoom;

import hf.Hf;
import hf.Mode;
import hf.mode.GameMode;
import etwin.ds.FrozenArray;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

@:build(patchman.Build.di())
class Zoom {

  @:diExport
  public var patches(default, null): IPatch;

  @:diExport
  public var actions(default, null): IAction;
    
  public var customZoom: Bool;
  public var customShakeTimerTotal: Float;
  public var customShakeTimer: Float;
  public var customShakeIntensity: Float;
  public var customShakeFlip: Bool;

  public function new() {
    this.actions = new Zooming(this);
      
    this.customZoom = false;

    this.patches = new PatchList([

      Ref.auto(hf.mode.GameMode.main).after(function(hf: hf.Hf, self: hf.mode.GameMode): Void {
        if (this.customShakeTimer > 0) {
          if (this.customShakeFlip && Std.random(2) == 1) {
            self.mc._xscale *= -1;
          }
          if (this.customShakeFlip && Std.random(2) == 1) {
            self.mc._yscale *= -1;
          }
          this.customShakeTimer -= self.root.Timer.tmod;
        }
        if (this.customShakeTimer <= 0) {
          this.customShakeTimer = 0;
          this.customShakeIntensity = 0;
          this.customShakeFlip = false;
          if (this.customZoom != true) {
            self.mc._xscale = 100;
            self.mc._yscale = 100;
          }
        }
        if (this.customZoom != true) {
          self.mc._x = Math.round(self.xOffset + (Std.random(2) * 2 - 1) * (Std.random(Math.round(this.customShakeIntensity * 10)) / 10)) + (self.mc._xscale < 0 ? self.root.Data.DOC_WIDTH - 20: 0);
          self.mc._y = Math.round(self.yOffset + (Std.random(2) * 2 - 1) * (Std.random(Math.round(this.customShakeIntensity * 10)) / 10)) + (self.mc._yscale < 0 ? self.root.Data.DOC_HEIGHT : 0);
        }
      }),
    ]);
  }
}

class Zooming implements IAction {
  public var name(default, null): String = Obfu.raw("zoom");
  public var isVerbose(default, null): Bool = false;

  private var mod: Zoom;
    
  public function new(mod: Zoom) {
    this.mod = mod;
  }

  public function run(ctx: IActionContext): Bool {
    var game: GameMode = ctx.getGame();
    var hf: Hf = ctx.getHf();
      
    var cx: Float = ctx.getOptFloat(Obfu.raw("x")).or(0);
    var cy: Float = ctx.getOptFloat(Obfu.raw("y")).or(0);
    var x: Float = game.flipCoordReal(ctx.getOptInt(Obfu.raw("xr")).or(_ => hf.Entity.x_ctr(cx)));
    var y: Float = ctx.getOptInt(Obfu.raw("yr")).or(_ => hf.Entity.y_ctr(cy));
    var zoom: Float = ctx.getOptFloat(Obfu.raw("zoom")).toNullable();
    var flip: Bool = ctx.getOptBool(Obfu.raw("flip")).or(false);

    this.zoom(game, x, y, zoom, flip);


    return false;
  }
    
  private function zoom(game: hf.mode.GameMode, x: Float, y: Float, zoom: Float, flip: Bool): Void {
    if (zoom == null) {
      game.mc._x = 10;
      game.mc._y = 0;
      game.mc._xscale = 100;
      game.mc._yscale = 100;
      this.mod.customZoom = false;
    } else {
      game.mc._x = 10 - (zoom - 1) * x - x + 210 + zoom * 10;
      game.mc._y = -(zoom - 1) * y - y + 260;
      game.mc._xscale = zoom * 100;
      game.mc._yscale = zoom * 100;
      if (flip) {
        game.mc._y = 500 - game.mc._y;
        game.mc._yscale *= -1;
      }
      this.mod.customZoom = true;
    }
  }
}