package display_texts;

import etwin.ds.FrozenArray;
import patchman.PatchList;
import patchman.IPatch;
import patchman.Ref;
import etwin.Obfu;
import merlin.IAction;
import merlin.IActionContext;

import hf.Hf;
import hf.entity.Player;
import hf.mode.GameMode;
import hf.FxManager;
import hf.Data;

import etwin.flash.MovieClip;

@:build(patchman.Build.di())
class DisplayTexts {

  @:diExport
  public var patches(default, null): IPatch;

  @:diExport
  public var actions(default, null): FrozenArray<IAction>;

  public var textList: Array<MovieClip>;

  public function new() {
    this.actions = FrozenArray.of(
      new RemoveTexts(this),
      new DisplayText(this)
    );

    this.textList = new Array();

    this.patches = new PatchList([

      Ref.auto(Player.onDeathLine).before(function(hf, self: Player): Void {
        if (!self.fl_kill && self.game.checkLevelClear()) {
            for(i in 0...textList.length) {
                textList[i].removeMovieClip();
            }
            textList.splice(0, textList.length);
        }
      }),

      Ref.auto(GameMode.forcedGoto).before(function(hf, self: GameMode, id): Void {
        for(i in 0...textList.length) {
            textList[i].removeMovieClip();
        }
        textList.splice(0, textList.length);
      }),

      Ref.auto(GameMode.switchDimensionById).before(function(hf, self: GameMode, id: Int, lid: Int, pid: Int): Void {
        for(i in 0...textList.length) {
            textList[i].removeMovieClip();
        }
        textList.splice(0, textList.length);
      }),

    ]);
  }

  public function createText(game, x, y, textSize, txt: String): Void {
    var hf: Hf = game.root;
    for(text in textList){
      if((cast text).field.text == txt)
        return null;
    }
    var Text: MovieClip = cast game.depthMan.attach('hammer_interf_inGameMsg', hf.Data.DP_SPRITE_BACK_LAYER);
    (cast Text).field._width = 400;
    (cast Text).label.text = "";
    (cast Text).field.text = txt;
    (cast Text).field._xscale = textSize;
    (cast Text).field._yscale = textSize;
    (cast Text).field._x = x;
    (cast Text).field._y = y;
    hf.FxManager.addGlow((cast Text), 0, 2);
    textList.push((cast Text));
  }
}

class RemoveTexts implements IAction {
  public var name(default, null): String = Obfu.raw("removeTexts");
  public var isVerbose(default, null): Bool = false;

  private var mod: DisplayTexts;

  public function new(mod: DisplayTexts) {
    this.mod = mod;
  }

  public function run(ctx: IActionContext): Bool {
    for(i in 0...this.mod.textList.length) {
      this.mod.textList[i].removeMovieClip();
    }
    this.mod.textList.splice(0, this.mod.textList.length);
    return false;
  }
}

class DisplayText implements IAction {
  public var name(default, null): String = Obfu.raw("displayText");
  public var isVerbose(default, null): Bool = false;

  private var mod: DisplayTexts;

  public function new(mod: DisplayTexts) {
    this.mod = mod;
  }

  public function run(ctx: IActionContext): Bool {
    var game = ctx.getGame();
    var X: Float = ctx.getOptInt(Obfu.raw("x")).or(0);
    var Y: Float = ctx.getOptInt(Obfu.raw("y")).or(0);
    var xr: Float = ctx.getOptInt(Obfu.raw("xr")).or(-1);
    var yr: Float = ctx.getOptInt(Obfu.raw("yr")).or(-1);
    var txt: String = ctx.getString(Obfu.raw("txt"));
    var textSize: Int = ctx.getOptInt(Obfu.raw("size")).or(100);
    
    var x: Float = 0;
    var y: Float = 0;

    if(xr == -1)
      x = X;
    else
      x = xr;

    if(yr == -1)
      y = Y;
    else
      y = yr;

    this.mod.createText(game, x, y, textSize, txt);
    return false;
  }
}