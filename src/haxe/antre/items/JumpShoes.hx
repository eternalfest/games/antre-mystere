package antre.items;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import hf.mode.GameMode;
import vault.ISpec;

class JumpShoes implements ISpec {
  public var id(default, null): Int = 119;

  public function new(): Void {}

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    specMan.permanent(item.id);
    hf.Data.PLAYER_JUMP = 33;
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
    specMan.clearRec();
    specMan.actives[this.id] = false;
    hf.Data.PLAYER_JUMP = 18.7;
  }
}