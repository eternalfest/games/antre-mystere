package antre.items;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import hf.mode.GameMode;
import vault.ISpec;
import antre.items.Ice;

class Nacho implements ISpec {
  public var id(default, null): Int = 118;
    
  private var ice(default, null): Ice;

  public function new(ice: Ice) {
    this.ice = ice;
  }

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    specMan.temporary(item.id, null);
    ice.enableIce(hf, specMan.game);
    specMan.game.fxMan.attachBg(14, null, 9999);
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
    specMan.clearRec();
    specMan.actives[this.id] = false;
    ice.disableIce(specMan.game);
  }
}