package antre.items;

import etwin.flash.MovieClip;
import hf.Hf;

class Snowflake extends MovieClip {
  public var sub(default, null): MovieClip;
  public var speed: Float;

  public function init(hf: Hf, x: Float, y: Float, speed: Float): Void {
    this._visible = true;
    this._alpha = 90 - hf.Std.random(20) + hf.Std.random(20);
    this._xscale = 40 + hf.Std.random(65);
    this._yscale = this._xscale;
    this._rotation = hf.Std.random(360);
    this._x = x;
    this._y = y;
    this.speed = speed;
  }
}
