package antre.actions;

import hf.Hf;
import hf.Mode;
import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;
import etwin.flash.MovieClip;
import etwin.flash.DynamicMovieClip;

class FakeCoeff implements IAction {
  public var name(default, null): String = Obfu.raw("fakeCoeff");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: IActionContext): Bool {
    var x: Float = ctx.getOptInt(Obfu.raw("x")).or(0);
    var y: Float = ctx.getOptInt(Obfu.raw("y")).or(0);
    var game: GameMode = ctx.getGame();
    var hf: Hf = ctx.getHf();
      
    var coeff: Array<Int> = new Array();
    coeff = [42, 44, 76, 78, 123, 131, 134, 143, 163, 164, 192, 213, 218];
    var random = hf.Std.random(12) + 1;
      
    var o: MovieClip = game.depthMan.attach("hammer_item_score", hf.Data.DP_INTERF);
    var obj: DynamicMovieClip = cast o;
    obj.gotoAndStop('' + (coeff[random] + 1));
    obj._x = x*20 + 20;
    obj._y = y*20 + 20;
    
    game.fxMan.mcList.push(obj);

    return false;
  }
}