package antre.actions;

import hf.Hf;
import hf.Mode;
import hf.mode.GameMode;
import hf.entity.Player;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class FixTilesAI implements IAction {
  public var name(default, null): String = Obfu.raw("recalculateAI");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: IActionContext): Bool {
    var y:  Int = ctx.getOptInt(Obfu.raw("y")).or(0);
    var y1: Int = ctx.getOptInt(Obfu.raw("y1")).or(y);
    var y2: Int = ctx.getOptInt(Obfu.raw("y2")).or(y);
    var game: GameMode = ctx.getGame();
    var hf: Hf = ctx.getHf();

    if(y1 > y2) {
      var tmp = y1;
      y1 = y2;
      y2 = tmp;
    }
    y2++;
      
    var world = game.world;
    var old = hf.Data.MAX_ITERATION;
    hf.Data.MAX_ITERATION = (y2 - y1) * hf.Data.LEVEL_WIDTH;
    Reflect.setField(world.manager, "progress", null);
    Reflect.setField(world, "onParseIAComplete", null);
      
    for(y in y1...y2) {
      for(x in 0...hf.Data.LEVEL_WIDTH) {
        world.flagMap[x][y] = 0;
        world.fallMap[x][y] = -1;
      }
    }
    world.parseCurrentIA({ cx: 0, cy: y1 });

    Reflect.deleteField(world.manager, "progress");
    Reflect.deleteField(world, "onParseIAComplete");
    hf.Data.MAX_ITERATION = old;

    return false;
  }
}
