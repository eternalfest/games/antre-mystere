package antre.actions;

import hf.Hf;
import hf.mode.GameMode;
import hf.entity.Player;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class ResetSpeed implements IAction {
  public var name(default, null): String = Obfu.raw("resetSpeed");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: IActionContext): Bool {
    var game: GameMode = ctx.getGame();
      
    for(player in game.getPlayerList()) {
        player.speedFactor = 1.3;
    }

    return false;
  }
}