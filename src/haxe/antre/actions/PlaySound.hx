package antre.actions;

import hf.Hf;
import hf.Mode;
import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class PlaySound implements IAction {
  public var name(default, null): String = Obfu.raw("playSound");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: IActionContext): Bool {
    var sound: String = ctx.getString(Obfu.raw("n"));
    var game: hf.mode.GameMode = ctx.getGame();
    
    this.playSound(game, sound);
    
    return false;
  }
    
  private function playSound(game: hf.mode.GameMode, sound: String): Void {
    if ((game.root.GameManager.CONFIG.hasMusic() && game.fl_mute == false) || game.root.GameManager.CONFIG.soundVolume > 0) {
      game.soundMan.playSound(sound, 7);
    }
  } 
}