package antre.actions;

import hf.Hf;
import hf.Mode;
import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;
import etwin.flash.MovieClip;
import etwin.flash.DynamicMovieClip;

class Radis implements IAction {
  public var name(default, null): String = Obfu.raw("radis");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: IActionContext): Bool {
    var x: Float = ctx.getOptInt(Obfu.raw("x")).or(0);
    var y: Float = ctx.getOptInt(Obfu.raw("y")).or(0);
    var flip: Bool = ctx.getOptBool(Obfu.raw("flip")).or(false);
    var game: GameMode = ctx.getGame();
    var hf: Hf = ctx.getHf();
      
    var r: MovieClip = game.depthMan.attach("hammer_item_score", hf.Data.DP_INTERF);
    var radis: DynamicMovieClip = cast r;
    radis.gotoAndStop('' + (39 + 1));
    radis._x = x*20 + 20;
    radis._y = y*20 + 20;
      
    if(flip) {
        radis._xscale *= -1;
    }
    
    game.fxMan.mcList.push(radis);

    return false;
  }
}