package antre.actions;

import hf.Hf;
import hf.Mode;
import hf.mode.GameMode;
import hf.entity.Player;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class KillPlayers implements IAction {
  public var name(default, null): String = Obfu.raw("killPlayers");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: IActionContext): Bool {
    var game: GameMode = ctx.getGame();

    for(player in game.getPlayerList()) {
        player.forceKill(null);
    }

    return false;
  }
}
