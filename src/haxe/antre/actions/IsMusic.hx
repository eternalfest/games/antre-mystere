package antre.actions;

import hf.Hf;
import hf.Mode;
import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class IsMusic implements IAction {
  public var name(default, null): String = Obfu.raw("isMusic");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: IActionContext): Bool {
    var orSound: Bool = ctx.getOptBool(Obfu.raw("orSound")).or(false);
    var game: GameMode = ctx.getGame();
    
    return this.isMusic(game, orSound);
  }
    
  private function isMusic(game: hf.mode.GameMode, orSound: Bool): Bool {
    if (game.root.GameManager.CONFIG.hasMusic() && game.fl_mute == false) {
      return true;
    }
    if (orSound && game.root.GameManager.CONFIG.soundVolume > 0) {
      return true;
    }
    return false;
  } 
}