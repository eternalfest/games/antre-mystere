package antre.quetes;

import merlin.value.MerlinValue;
import merlin.Merlin;
import hf.mode.GameMode;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import etwin.Obfu;

@:build(patchman.Build.di())
class QuestsInit {

  @:diExport
  public var patch(default, null): IPatch;
    
  public function new(): Void {
    this.patch = new PatchList([
      Ref.auto(hf.mode.GameMode.initGame).before(function(hf, self: GameMode) {
        Merlin.setGlobalVar(self, Obfu.raw("SPEED"), cast false);
        Merlin.setGlobalVar(self, Obfu.raw("TABASCO"), cast false);
        Merlin.setGlobalVar(self, Obfu.raw("REPAS"), cast false);
        Merlin.setGlobalVar(self, Obfu.raw("NACHO"), cast false);
        Merlin.setGlobalVar(self, Obfu.raw("CHOCOREMS"), cast false);
      }),
    ]);
  }
}