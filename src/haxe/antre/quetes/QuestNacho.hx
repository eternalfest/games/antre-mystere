package antre.quetes;

import user_data.RewardItem;
import quests.QuestReward;
import merlin.value.MerlinValue;
import etwin.Obfu;
import merlin.Merlin;
import hf.mode.GameMode;

class QuestNacho implements QuestReward {

  public function new() {}

  public function give(game: GameMode, reward: RewardItem): Void {
    Merlin.setGlobalVar(game, Obfu.raw("NACHO"), cast true);
  }

  public function remove(game: GameMode, reward: RewardItem): Void {
    Merlin.setGlobalVar(game, Obfu.raw("NACHO"), cast false);
  }
}
