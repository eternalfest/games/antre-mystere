package antre.quetes;

import user_data.RewardItem;
import quests.QuestReward;
import merlin.value.MerlinValue;
import etwin.Obfu;
import merlin.Merlin;
import hf.mode.GameMode;

class QuestChocorems implements QuestReward {

  public function new() {}

  public function give(game: GameMode, reward: RewardItem): Void {
    Merlin.setGlobalVar(game, Obfu.raw("CHOCOREMS"), cast true);
  }

  public function remove(game: GameMode, reward: RewardItem): Void {
    Merlin.setGlobalVar(game, Obfu.raw("CHOCOREMS"), cast false);
  }
}
