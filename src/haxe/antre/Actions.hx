package antre;

import etwin.ds.FrozenArray;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import merlin.IAction;
import merlin.IRefFactory;
import antre.actions.KillPlayers;
import antre.actions.FixTilesAI;
import antre.actions.Radis;
import antre.actions.IsMusic;
import antre.actions.PlaySound;
import antre.actions.FakeCoeff;
import antre.actions.ResetSpeed;

@:build(patchman.Build.di())
class Actions {
  @:diExport
  public var actions(default, null): FrozenArray<IAction>;

  @:diExport
  public var patches(default, null): FrozenArray<IPatch>;
  @:diExport
  public var killPlayers(default, null): IAction;
  @:diExport
  public var fixTilesAI(default, null): IAction;
  @:diExport
  public var radis(default, null): IAction;
  @:diExport
  public var isMusic(default, null): IAction;
  @:diExport
  public var playSound(default, null): IAction;
  @:diExport
  public var fakeCoeff(default, null): IAction;
  @:diExport
  public var resetSpeed(default, null): IAction;
    
  public function new() {
    this.patches = FrozenArray.from(patches);
    this.killPlayers = new KillPlayers();
    this.fixTilesAI = new FixTilesAI();
    this.radis = new Radis();
    this.isMusic = new IsMusic();
    this.playSound = new PlaySound();
    this.fakeCoeff = new FakeCoeff();
    this.resetSpeed = new ResetSpeed();
  }
}
