package antre;

import etwin.ds.FrozenArray;
import patchman.IPatch;
import vault.ISpec;
import vault.ISkin;

import antre.items.Boots;
import antre.items.JumpShoes;
import antre.items.Nacho;
import antre.items.PieceViolette;

import antre.items.Ice;

@:build(patchman.Build.di())
class Items {
  @:diExport
  public var items(default, null): FrozenArray<ISpec>;
  @:diExport
  public var skins(default, null): FrozenArray<ISkin>;

  @:diExport
  public var patches(default, null): FrozenArray<IPatch>;

  public function new(ice: Ice, patches: Array<IPatch>) {
    this.patches = FrozenArray.from(patches);
    this.items = FrozenArray.of(
      new Boots(),
      new JumpShoes(),
      new Nacho(ice)
    );
      
    this.skins = FrozenArray.of(
      (new PieceViolette(): ISkin)
    );
  }
}