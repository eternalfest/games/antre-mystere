package pieces_violettes;

import patchman.IPatch;
import patchman.Ref;
import merlin.IAction;
import etwin.ds.FrozenArray;
import etwin.Obfu;
import etwin.flash.MovieClip;
import etwin.ds.WeakMap;
import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;
import hf.Hf;
import hf.FxManager;
import hf.mode.GameMode;
import event_timer.EventTimer;

import pieces_violettes.actions.PieceViolette;
import pieces_violettes.actions.PiecesDisplay;
import pieces_violettes.actions.PiecesDisplayStop;
import pieces_violettes.actions.PiecesDisplayReinit;
import pieces_violettes.actions.PiecesReussies;

@:build(patchman.Build.di())
class PiecesViolettes {

  @:diExport
  public var actions(default, null): FrozenArray<IAction>;
  @:diExport
  public var patches(default, null): FrozenArray<IPatch>;
  
  public static var PIECE_ID = 1243;
  public static var PIECES_ACTIVATE: WeakMap<GameMode, Bool> = new WeakMap();
  public var piecesWin: Bool;
  public var nbPieces: Int;
  public var curKeyText: MovieClip;

  public function new() {
    this.actions = FrozenArray.of(
      new PieceViolette(this),
      new PiecesDisplay(this),
      new PiecesDisplayStop(this),
      new PiecesDisplayReinit(this),
      new PiecesReussies(this)
    );

    var patches = [
        
      Ref.auto(hf.mode.GameMode.pickUpScore).wrap(function(hf, self: GameMode, id: Int, subId: Int, old): Int {
        if(id == PIECE_ID - 1000 && PIECES_ACTIVATE.get(self)) {
          nbPieces++;
          this.piecesDisplay(hf, self);
        }
        return old(self, id, subId);
      }),
         
    ];
      
    this.patches = FrozenArray.from(patches);
  }
    
  public function piecesDisplay(hf: Hf, self: GameMode): Void {
    curKeyText.removeMovieClip();
    curKeyText = cast self.depthMan.attach('hammer_interf_inGameMsg', hf.Data.DP_TOP);
    (cast curKeyText).field._width = 400;
    (cast curKeyText).label.text = "";
    
    if(nbPieces < 10) {
      (cast curKeyText).field.text = "00" + nbPieces;
    } else if(nbPieces >= 10 && nbPieces < 100) {
      (cast curKeyText).field.text = "0" + nbPieces;
    } else {
      (cast curKeyText).field.text = nbPieces;
      (cast curKeyText).field.textColor = 11337966;
    }
      
    if(nbPieces == 100 && !piecesWin) {
      this.piecesDisplayMessageCentre(hf, self, "100 pièces violettes attrapées !");
      piecesWin = true;
    }
    (cast curKeyText).field._xscale = 130;
    (cast curKeyText).field._yscale = 130;
    (cast curKeyText).field._x = 24;
      
    var piece = hf.Std.attachMC(curKeyText, 'hammer_item_score', self.manager.uniq);
    piece.gotoAndStop('' + (51 + 1));
    piece._x = 17;
	piece._xscale = 80;
	piece._yscale = 80;
    var filter: ColorMatrixFilter = ColorMatrix.fromHsv(135, 1, 1).toFilter();
    piece.filters = [filter];
      
    if(EventTimer.TIMER_ACTIVATE.get(self)) {
      (cast curKeyText).field._y = 25;
      piece._y = 44;
    } else {
      (cast curKeyText).field._y = 0;
      piece._y = 19;
    }
      
    hf.FxManager.addGlow(curKeyText, 0, 2);
  }
    
  public function piecesDisplayMessageCentre(hf: Hf, self: GameMode, txt: String): MovieClip {
    var msg = cast self.depthMan.attach('hurryUp', hf.Data.DP_INTERF);
    (cast msg)._x = hf.Data.GAME_WIDTH / 2;
    (cast msg)._y = hf.Data.GAME_HEIGHT / 2;
    (cast msg).label = txt;
    (cast msg)._xscale = 100;
	(cast msg)._yscale = 100;
    self.fxMan.mcList.push(msg);
    self.fxMan.lastAlert = msg;
    return msg;
  }

  public function piecesRemove(): Void {
    this.curKeyText.removeMovieClip();
  }

}