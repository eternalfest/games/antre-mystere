package pieces_violettes.actions;

import hf.Hf;
import hf.mode.GameMode;
import hf.entity.item.ScoreItem;
import etwin.ds.WeakMap;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class PiecesReussies implements IAction {
  public var name(default, null): String = Obfu.raw("piecesReussies");
  public var isVerbose(default, null): Bool = false;
    
  private var mod: PiecesViolettes;

  public function new(mod: PiecesViolettes) {
    this.mod = mod;
  }

  public function run(ctx: IActionContext): Bool {
    var limitPieces: Int = ctx.getOptInt(Obfu.raw("pieces")).or(100);
    var more: Bool = ctx.getOptBool(Obfu.raw("more")).or(false);
    var game: GameMode = ctx.getGame();
    var hf: Hf = ctx.getHf();
    
    var result = false;
    if(PiecesViolettes.PIECES_ACTIVATE.get(game)) {
      if(more) {
        if(this.mod.nbPieces >= limitPieces) result = true;
      } else {
        if(this.mod.nbPieces == limitPieces) result = true;
      }
    }

    return result;
  }
}