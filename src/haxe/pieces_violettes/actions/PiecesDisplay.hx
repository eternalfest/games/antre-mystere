package pieces_violettes.actions;

import hf.Hf;
import hf.mode.GameMode;
import hf.entity.item.ScoreItem;
import etwin.ds.WeakMap;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class PiecesDisplay implements IAction {
  public var name(default, null): String = Obfu.raw("piecesDisplay");
  public var isVerbose(default, null): Bool = false;
    
  private var mod: PiecesViolettes;

  public function new(mod: PiecesViolettes) {
    this.mod = mod;
  }

  public function run(ctx: IActionContext): Bool {
    var game: GameMode = ctx.getGame();
    var hf: Hf = ctx.getHf();
    
    mod.nbPieces = 0;
    PiecesViolettes.PIECES_ACTIVATE.set(game, true);
    mod.piecesWin = false;
    mod.piecesDisplay(hf, game);

    return false;
  }
}