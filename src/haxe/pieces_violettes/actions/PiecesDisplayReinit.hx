package pieces_violettes.actions;

import hf.Hf;
import hf.mode.GameMode;
import hf.entity.item.ScoreItem;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class PiecesDisplayReinit implements IAction {
  public var name(default, null): String = Obfu.raw("piecesDisplayReinit");
  public var isVerbose(default, null): Bool = false;
    
  private var mod: PiecesViolettes;

  public function new(mod: PiecesViolettes) {
    this.mod = mod;
  }

  public function run(ctx: IActionContext): Bool {
    var game: GameMode = ctx.getGame();
    var hf: Hf = ctx.getHf();
    
    mod.piecesDisplay(hf, game);

    return false;
  }
}