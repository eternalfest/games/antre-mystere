package pieces_violettes.actions;

import hf.Hf;
import hf.mode.GameMode;
import hf.entity.item.ScoreItem;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class PieceViolette implements IAction {
  public var name(default, null): String = Obfu.raw("pieceViolette");
  public var isVerbose(default, null): Bool = false;
    
  private var mod: PiecesViolettes;

  public function new(mod: PiecesViolettes) {
    this.mod = mod;
  }

  public function run(ctx: IActionContext): Bool {
    var x: Int = ctx.getOptInt(Obfu.raw("x")).or(0);
    var y: Int = ctx.getOptInt(Obfu.raw("y")).or(0);
    var game: GameMode = ctx.getGame();
    var hf: Hf = ctx.getHf();
    
    x = x*20 + 10;
    y = y*20 + 20;
 
    var piecesViolette = hf.entity.item.ScoreItem.attach(game, x, y, PiecesViolettes.PIECE_ID, null);
    piecesViolette.fl_gravity = false;
    piecesViolette.setLifeTimer(-1);

    return false;
  }
}