package pieces_violettes.actions;

import hf.Hf;
import hf.mode.GameMode;
import hf.entity.item.ScoreItem;
import etwin.ds.WeakMap;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class PiecesDisplayStop implements IAction {
  public var name(default, null): String = Obfu.raw("piecesDisplayStop");
  public var isVerbose(default, null): Bool = false;
    
  private var mod: PiecesViolettes;

  public function new(mod: PiecesViolettes) {
    this.mod = mod;
  }

  public function run(ctx: IActionContext): Bool {
    var game: GameMode = ctx.getGame();
    PiecesViolettes.PIECES_ACTIVATE.set(game, false);
    mod.piecesRemove();

    return false;
  }
}