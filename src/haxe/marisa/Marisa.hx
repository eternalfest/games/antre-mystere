package marisa;

import marisa.actions.MarisaAction;
import patchman.IPatch;
import patchman.Ref;
import merlin.IAction;
import etwin.ds.FrozenArray;
import etwin.Obfu;
import color_matrix.ColorMatrix;

@:build(patchman.Build.di())
class Marisa {

  @:diExport
  public var actions(default, null): FrozenArray<IAction>;
  @:diExport
  public var patches(default, null): FrozenArray<IPatch>;

  public var marisa: Dynamic;
  public var mastersparkCircle: Dynamic;
  public var mastersparkBeams: Array<Dynamic>;
  public var mastersparkStars: Array<Dynamic>;

  public function new() {
    this.actions = FrozenArray.of(
      (new MarisaAction(this): IAction)
    );

    var patches = [
      Ref.auto(hf.levels.GameMechanics.update).before(function(hf: hf.Hf, self: hf.levels.GameMechanics) {
          
        if (!(self.game.fl_lock && self.game.world.fl_lock)) {
          
          if (this.marisa.state == 1) {
            if (this.marisa.speed > 0) {
              this.marisa.speed -= 0.25;
            } 
            else {
              this.marisa.state = 2;
              this.marisa.count = 0;
              this.marisa.defaultY = this.marisa._y;
            }
            this.marisa._y += this.marisa.speed;
          }
        
          else if (this.marisa.state == 2) {
            this.marisa._y = this.marisa.defaultY + 3 * -Math.sin(this.marisa.count / 10);
            this.marisa.count++;
          }
          
          else if (this.marisa.state == 3) {
            self.game.getPlayerList()[0].dir = -1;
            self.game.getPlayerList()[0].dx = -1;
            self.game.getPlayerList()[0].speedFactor = 1;
            this.mastersparkBeams = [];
            for (i in 0...4) {
              this.mastersparkBeams.push(self.game.world.view.attachSprite(Obfu.raw("masterspark_beam"), 210, 160, false));
            }
            for (i in 0...4) {
              this.mastersparkBeams[i]._xscale = 150 - 35 * i;
              var glowFilter = new etwin.flash.filters.GlowFilter();
              glowFilter.color = 0xFFFFFF;
              glowFilter.quality = 3;
              glowFilter.strength = 100;
              glowFilter.blurX = 3;
              glowFilter.blurY = 3;
              Obfu.setField(glowFilter, "alpha", 0.4);
              this.mastersparkBeams[i].filters = [glowFilter];
              this.mastersparkBeams[i]._visible = true;
            }
            this.marisa.state = 4;
          }
        
          else if (this.marisa.state == 4) {}
        
          else if (this.marisa.state == 5) {
            for (i in 0...4) {
              this.mastersparkBeams[i]._visible = false;
            }
            self.game.getPlayerList()[0]._visible = false;
            self.game.getPlayerList()[0].y = -10000;
            this.marisa.state = 4;
          }
          
          else if (this.marisa.state == 6) {
            self.game.getPlayerList()[0]._visible = true;
            self.game.getPlayerList()[0].speedFactor = 1.3;
            this.marisa.state = 4;
          }
          
          else if (this.marisa.state == 7) {
            self.game.getPlayerList()[0].dir = -1;
            self.game.getPlayerList()[0].dx = -1;
            self.game.getPlayerList()[0].speedFactor = 1; 
            self.game.getPlayerList()[0].playAnim(hf.Data.ANIM_PLAYER_WALK);
          }
        }
      }),
    ];
      
    this.patches = FrozenArray.from(patches);
  }

  public function setMarisa(game: hf.mode.GameMode, state: Int): Void {
    this.marisa = game.world.view.attachSprite(Obfu.raw('marisa'), 190, -80, false);
    this.marisa.speed = 10;
    this.marisa.state = state;
    game.world.scriptEngine.mcList.push({sid: 1, mc: this.marisa});
    game.getPlayerList()[0].dir = -1;
  }
}