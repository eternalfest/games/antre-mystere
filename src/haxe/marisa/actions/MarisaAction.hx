package marisa.actions;

import etwin.Obfu;
import merlin.IAction;
import merlin.IActionContext;

class MarisaAction implements IAction {
  public var name(default, null): String = Obfu.raw("marisa");
  public var isVerbose(default, null): Bool = false;

  private var mod: Marisa;

  public function new(mod: Marisa) {
    this.mod = mod;
  }

  public function run(ctx: IActionContext): Bool {
    var game: hf.mode.GameMode = ctx.getGame();
    var state: Int = ctx.getInt(Obfu.raw("v"));

    this.mod.setMarisa(game, state);

    return false;
  }
}